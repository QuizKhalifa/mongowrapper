package mongowrapper

import (
	"log"
	"reflect"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// InsertOne inserts one document into the database.
// It returns it's unique id and an error depending on everything went well
func (m *mongoDB) InsertOne(database, coll string, query interface{}) (string, error) {
	collection := m.GetCollection(database, coll)
	insertedObject, err := collection.InsertOne(m.Ctx, query)

	return insertedObject.InsertedID.(primitive.ObjectID).Hex(), err
}

// UpsertOne upserts the document matching 'filter'
func (m *mongoDB) UpsertOne(database, coll string, filter, query interface{}) error {
	option := options.Update().SetUpsert(true)
	collection := m.GetCollection(database, coll)
	_, err := collection.UpdateOne(m.Ctx, filter, bson.M{"$set": query}, option)

	return err
}

// UpsertByID upserts documents based on their ID into the database
// upsert = update or insert
func (m *mongoDB) UpsertByID(database, coll string, insert interface{}) error {
	var err error
	collection := m.GetCollection(database, coll)

	option := options.Update().SetUpsert(true)
	s := reflect.ValueOf(insert)
	for i := 0; i < s.Len(); i++ {
		filter := bson.M{"ID": s.Index(i).FieldByName("ID").Int()}
		_, err := collection.UpdateOne(m.Ctx, filter, bson.M{"$set": s.Index(i).Interface()}, option)
		if err != nil {
			log.Printf("An error has occurred: %s", err)
		}
	}
	return err
}

// GetOne gets a single document matching the filter,
// decodes it into 'dest' and returns an error if something went wrong
func (m *mongoDB) GetOne(database, coll string, filter interface{}, dest interface{}) error {
	collection := m.GetCollection(database, coll)
	document := collection.FindOne(m.Ctx, filter)
	return document.Decode(dest)
}

// Get gets all documents that matches the filter
// To do something with this information, specify a function 'f func(interface{})' that handles each document
func (m *mongoDB) Get(database, coll string, filter, destType interface{}, f func(interface{})) error {
	collection := m.GetCollection(database, coll)

	iter, err := collection.Find(m.Ctx, filter)

	if err == nil {
		for iter.Next(m.Ctx) {
			elem := reflect.New(reflect.TypeOf(destType))
			err := iter.Decode(elem.Interface())
			if err == nil {
				f(reflect.Indirect(elem).Interface())
			} else {
				break
			}
		}
	}
	return err
}

// DeleteOne deletes a document matching the filter from the db
func (m *mongoDB) DeleteOne(database, coll string, filter interface{}) error {
	collection := m.GetCollection(database, coll)
	_, err := collection.DeleteOne(m.Ctx, filter)

	return err
}
