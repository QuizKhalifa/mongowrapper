package mongowrapper

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type database interface {
	Init()
	Setup()
	InsertOne(database, coll string, query interface{})
	UpsertByID(database, coll string, insert interface{})
	GetOne(database, coll string, filter interface{})
	Get(database, coll string, filter, destType interface{}, f func(interface{}))
	DeleteOne()
	KeepDBUpdated()
	GetCollection()
}

type mongoDB struct {
	Ctx        context.Context
	CancelFunc func()
	Client     *mongo.Client
}

// newMongoDB creates a new initialized mongoDB object.
func NewMongoDB(url, port string) (mongoDB, error) {
	resp := mongoDB{}
	err := resp.Init(url, port)
	return resp, err
}
