package mongowrapper

import (
	"context"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Intializes the object by connecting to the database, and setting up a context.
func (m *mongoDB) Init(url, port string) error {
	// Setup mongodb
	var err error
	clientOptions := options.Client().ApplyURI("mongodb://" + url + ":" + port)
	m.Client, err = mongo.Connect(context.TODO(), clientOptions)
	m.Ctx, m.CancelFunc = context.WithCancel(context.Background())

	if err != nil {
		return errors.Wrap(err, "Error in mongoDB.Init()")
	}
	if GetResponse("http://"+url+":"+port) != http.StatusOK {
		return errors.New("Cannot connect to database")
	}
	return nil
}

// getCollection creates a context and creates a collection ready to talk to the database
func (m *mongoDB) GetCollection(database, coll string) *mongo.Collection {
	collection := m.Client.Database(database).Collection(coll)
	return collection
}

// GetResponse get just the response code from the url
func GetResponse(url string) int {
	var myClient = &http.Client{Timeout: 30 * time.Second}
	r, err := myClient.Get(url) // Check if its available

	if err == nil {
		defer r.Body.Close()
		return r.StatusCode
	}
	return http.StatusServiceUnavailable
}
