module bitbucket.org/QuizKhalifa/mongowrapper

go 1.15

require (
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.4.1
)
